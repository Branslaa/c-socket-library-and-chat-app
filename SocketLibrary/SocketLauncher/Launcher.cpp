/*
Brandon Slaa
Socket Launcher
*/
#include<iostream>
#include<string>
#include <vector>
#include <thread>

using namespace std;

#include <process.h>
#include <Windows.h>

unsigned nThreads = 5;

/**
Launches the client and server process that are located at the current directory.

@param wstring - The name of the current path and file.
@return void
*/
void launch(wstring fileApp) {
	wstring application = fileApp;
	wstring params = L"C:/setup.log";
	wstring command = application + L" " + params;

	cout << "CreateProcess() launching" << endl;
	STARTUPINFOW sinfo = { 0 };
	sinfo.cb = sizeof(STARTUPINFOW);
	PROCESS_INFORMATION pi = { 0 };
	unsigned long const CP_MAX_COMMANDLINE = 32768;
	try {
		wchar_t* commandline = new wchar_t[CP_MAX_COMMANDLINE];
		wcsncpy_s(commandline, CP_MAX_COMMANDLINE, command.c_str(), command.size() + 1);
		CreateProcessW(
			NULL,			// application name is null since it is provided in command line
			commandline,	// contains the application name and parameters
			NULL,			// same security level as parent (this program)
			NULL,			// start with normal runtime levels
			false,			// don't inherit handles
			CREATE_NEW_CONSOLE,				// defaults (Look up options: homework)
			NULL,
			NULL,			// same as parent
			&sinfo,
			&pi
		);

		delete[]commandline;
	}
	catch (bad_alloc&) {
		wcerr << L"Insufficient memory to launch the application." << endl;
		return;
	}

	CloseHandle(pi.hThread);
	CloseHandle(pi.hProcess);
	cout << "CreateProcess() landing" << endl;
}

int main() {

	vector<thread> threads;
	threads.push_back(thread(launch, L"SocketServer.exe"));
	threads.push_back(thread(launch, L"SocketClient.exe"));

	//Clean Up
	for (auto& t : threads)
		t.join();
}