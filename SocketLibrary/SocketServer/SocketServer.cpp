/*
Brandon Slaa
Socket Library Server
*/

#include "../SocketLibrary/SocketLib.cpp"
#include <cstdlib>
#include <ctime>
#include <algorithm>

using namespace std;

/**
A Random number generator

@return returns a random int.
*/
int getRandomNumber() {
	srand(time(0));
	return (rand() % 3 + 1);
}

/**
Takes the user input and the random number and uses them for RPS game.

@param string message - The text the user inputed.
@param int number - the random number the server generated.
@return returns a string to sent back to the client with the result.
*/
string playGame(string message, int number) {
	string result;
	if (message == "rock") {
		if (number == 1) {
			result = "You said " + message + " and they said rock, You have tied! So Close!";
		}
		else if (number == 2) {
			result = "You said " + message + " and they said paper, You have lost! Better luck next time!";
		}
		else if (number == 3) {
			result = "You said " + message + " and they said scissor, You have WON! Congratulations!";
		}
	}
	else if (message == "paper") {
		if (number == 1) {
			result = "You said " + message + " and they said rock, You have WON! Congratulations!";
		}
		else if (number == 2) {
			result = "You said " + message + " and they said paper, You have tied! So Close!";
		}
		else if (number == 3) {
			result = "You said " + message + " and they said scissor, You have lost! Better luck next time!";
		}
	}
	else if (message == "scissor") {
		if (number == 1) {
			result = "You said " + message + " and they said rock, You have lost! Better luck next time!";
		}
		else if (number == 2) {
			result = "You said " + message + " and they said paper, You have WON! Congratulations!";
		}
		else if (number == 3) {
			result = "You said " + message + " and they said scissor, You have tied! So Close!";
		}
	}
	else {
		result = "I am sorry, that is not a valid response.";
	}

	result += "\n\nPlease select either Rock, Paper or Scissor\n";
	return result;
}

int main() {
	SocketLibServer sServer;

	string message, result;
	for (;;) {
		message = sServer.recv();

		cout << "Client played " << message << endl;

		transform(message.begin(), message.end(), message.begin(), ::tolower);
		if (message != "!quit") {
			int number = getRandomNumber();

			string result = playGame(message, number);
			sServer.sendMsg(result, result.size());
		}
		else { break; }
	}
}