/*
Brandon Slaa
Socket Library Client
*/

#include "../SocketLibrary/SocketLib.cpp"
#include <iostream>
#include <string>
using namespace std;

int main() {
	SocketLibClient cSocket;

	int const MAXLINE = 256;
	char recvline[MAXLINE + 1];
	string line;

	cout << "Welcome to Rock! Paper! Scissor!\nTo get started, type Rock, Paper or Scissor to get started.\nIf you wish to end, please type in !quit" << endl;

	// Contunual loop until the user types !quit, will pass messages to the server and get a response.
	while (getline(cin, line)) {
		if (line.size() < MAXLINE) {
			if (line != "!quit") {
				cSocket.sendMsg(line);
				string recievedMessage = cSocket.recieveMessage();
				if (recievedMessage != "no reply") {
					cout << recievedMessage << endl;
				}
				else {
					cout << "I am sorry, the server does not seem to be responding." << endl;
				}

			}
			else {
				cSocket.sendMsg("!quit");
				break;
			}
		}
		else {
			cout << "Please enter a shorter string" << endl;
		}

	}
}