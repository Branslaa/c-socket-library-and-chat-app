/*
Brandon Slaa
Socket Library
*/
#include "SocketLib.hpp"

using namespace std;

/**
Recieves input from the server and passes the message to the client.

@return string that is sent to the client.
*/
string SocketLibServer::recv() {
	int const MAXLINE = 256;
	char msg[MAXLINE];

	int n = recvfrom(serverSocket, msg, MAXLINE, 0, &clientAddress, &cbClientAddress);
	msg[0] = toupper(msg[0]);
	msg[min(n, MAXLINE - 1)] = 0;
	return msg;
}

/**
Sends a message to the server.

@param string msg - The message that you want to send.
@param int n - The length of the message, defaults to 0.
@return void
*/
void SocketLibServer::sendMsg(string msg, int n = 0) {
	sendto(serverSocket, msg.c_str(), n, 0, &clientAddress, cbClientAddress);
}

/**
Sends a message to the client.

@param string msg - The message that you want to send.
@return void
*/
void SocketLibClient::sendMsg(string line) {
	sendto(clientSocket, line.c_str(), line.size(), 0, (sockaddr*)&serverAddress, sizeof(serverAddress));

}

/**
Recieves a message from the client.

@param N/A
@return string - the message that was sent.
*/
string SocketLibClient::recieveMessage() {
	int const MAXLINE = 256;
	char recvline[MAXLINE + 1];

	int clientSent = recvfrom(clientSocket, recvline, MAXLINE, 0, NULL, NULL);
	if (clientSent == -1) {
		return "no reply";
	}
	recvline[clientSent] = 0;
	string msg = recvline;

	return msg;
}