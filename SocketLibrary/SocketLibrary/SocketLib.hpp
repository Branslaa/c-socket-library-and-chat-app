/*
Brandon Slaa
Socket Library Header
*/
#pragma once

#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include <iostream>
#include <string>
using namespace std;

#include <WinSock2.h>
#include <WS2tcpip.h>
#pragma comment (lib,"ws2_32.lib")

// Main Socket Class
class SocketLibMain {
public:
	//Sockets and const port.
	SOCKET serverSocket, clientSocket;
	unsigned short const PORT = 27015;

	// Constructor
	SocketLibMain() {
		// initialize WSA
		WSAData wsaData;
		int iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
		if (iResult != 0) {
			cerr << "WSAStartup failed: " << iResult << endl;
		}

		SOCKET hSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	}

	//Destructor that cleans up the WSA Data
	virtual ~SocketLibMain() {
		WSACleanup();
	}
};

// Server Socket Class
class SocketLibServer : public SocketLibMain {
public:
	//Address for the Client
	sockaddr_in serverAddress;
	sockaddr clientAddress;
	socklen_t cbClientAddress = sizeof(clientAddress);

	// Constructor
	SocketLibServer() {
		serverSocket = socket(AF_INET, SOCK_DGRAM, 0);
		serverAddress = { 0 };
		serverAddress.sin_family = AF_INET;
		serverAddress.sin_port = htons(PORT);
		serverAddress.sin_addr.s_addr = htonl(INADDR_ANY);

		int res = bind(serverSocket, reinterpret_cast<sockaddr*>(&serverAddress), sizeof(sockaddr_in));
		if (res == SOCKET_ERROR) {
			cerr << "Error on bind\n";
			res = WSAGetLastError();
			cout << "Result: " << res << endl;
			closesocket(serverSocket);
		}
		cout << "UDP socket bound\n";
	}

	//Destructor that cleans up the WSA Data and Socket
	virtual ~SocketLibServer() {
		closesocket(serverSocket);
		WSACleanup();
	}

	string recv();
	void sendMsg(string, int);
};

class SocketLibClient : public SocketLibMain {
public:
	// Address for server
	sockaddr_in serverAddress;

	// Constructor
	SocketLibClient() {
		clientSocket = socket(AF_INET, SOCK_DGRAM, 0);

		// Create the server address
		serverAddress = { 0 };
		serverAddress.sin_family = AF_INET;
		serverAddress.sin_port = htons(PORT);
		serverAddress.sin_addr.s_addr = inet_addr("127.0.0.1");
	}

	//Destructor that cleans up the WSA Data and Socket
	virtual ~SocketLibClient() {
		closesocket(clientSocket);
		WSACleanup();
	}

	void sendMsg(string);
	string recieveMessage();
};